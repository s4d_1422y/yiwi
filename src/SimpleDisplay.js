import React, { Component } from 'react';
import './SimpleDisplay.css';

class SimpleDisplay extends Component {

    newCanvas(rx, ry) {
        var screen = Array(ry);
        for (var y=0; y<ry; y++){
            screen[y] = (
                <div className="line">
                    {this.props.content[y].map((char) => <div className="cell"> {char} </div>)}
                </div>
            )
        }
        return screen;
    }
    render() {
        var canvas = this.newCanvas(128, 32);
        return(
            <div className="display">
                <h1 id="yiwi">
                    YIWI COMPUTATION MACHINE EMULATION VIRTUAL SYSTEM.
                    VERSION 0.1
                </ h1>
                {canvas}
            </div>
        );
    }
}

export default SimpleDisplay;
