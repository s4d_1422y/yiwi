class Device { //all 16 bit => all memory = 65535*32 = H1FFFE0
    constructor(deviceID){
        this.id = deviceID;
        this.internalBuffer = Array(4).fill(0);
        this.readyForTransaction = true;
        this.connectionPool = Array(4).fill(0);
    }

    reset(){
        this.internalBuffer = Array(16).fill(0x0);
    }
    acceptTransaction(fromID){
        if (this.connectionPool[fromID].readyForTransaction &&
            this.readyForTransaction) {
            this.readyForTransaction = false;
                this.internalBuffer.push(this.connectionPool[fromID].connectorValue);
            this.readyForTransaction = true
        }
    }
    preformTransaction(toID){
        if (this.connectionPool[toID].readyForTransaction &&
            this.readyForTransaction) {
            this.readyForTransaction = false;
                this.connectorValue = this.internalBuffer.pop(); //
            this.readyForTransaction = true;
        }
    }
    connect(deviceTo){
        this.connectionPool[deviceTo.id] = deviceTo;
    }
    disconnect(deviceFrom){
        this.connectionPool[deviceFrom.id] = 0;
    }
}

export default Device;
