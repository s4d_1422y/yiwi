import React, { Component } from 'react';
import SimpleDisplay from './SimpleDisplay';
import TDIU from './tdiu';
import FADSS from './fadss';
import SADSU from './sadsu';
import Device from './Device';
import Ass from './AssLang';
import './App.css';

class App extends Component {
    initsys(){
        this.sadsu = new SADSU(0, (0xffff*32));
        this.fadss = new FADSS(1);
        this.tdiu = new TDIU(2, this.sadsu.tape);
        //
        var td = new Device(8);
        this.sadsu.connect(td);
        td.connect(this.sadsu);
        td.internalBuffer.push(0x04);
        td.internalBuffer.push(54);
        td.preformTransaction(0);
        this.sadsu.acceptTransaction(8);
        td.preformTransaction(0);
        this.sadsu.acceptTransaction(8);
        this.sadsu.processCommand();
        td.internalBuffer.push(0x02);
        td.internalBuffer.push(1);
        td.preformTransaction(0);
        this.sadsu.acceptTransaction(8);
        td.preformTransaction(0);
        this.sadsu.acceptTransaction(8);
        this.sadsu.processCommand();
        td.internalBuffer.push(0x04);
        td.internalBuffer.push(62);
        td.preformTransaction(0);
        this.sadsu.acceptTransaction(8);
        td.preformTransaction(0);
        this.sadsu.acceptTransaction(8);
        this.sadsu.processCommand();
        this.tdiu.refresh();
    }
    assdemo(mem){
        var basiclisting = `psh d72
psh d87
psh d33
sbr h1
pop
mov @10003 ^10000
inc @10000
mov @10000 h1
jsr h1
jsr h1
jsr h1`
            var ass = new Ass();
            var td = new Device(16);
            td.connect(this.fadss);
            this.fadss.connect(td);
            td.internalBuffer.push(0x02);
            td.internalBuffer.push(0x10000);
            td.internalBuffer.push(0xfff);
            td.preformTransaction(this.fadss.id);
            this.fadss.acceptTransaction(16);
            td.preformTransaction(this.fadss.id);
            this.fadss.acceptTransaction(16);
            td.preformTransaction(this.fadss.id);
            this.fadss.acceptTransaction(16);
            this.fadss.processCommand();
            var compilled = ass.memify(this.sadsu, this.fadss, ass.toOpCodes(basiclisting));
            var bs = [];
            compilled.map((elem) => elem.map((subelem) => bs.push(subelem)));
            bs = ass.bytify(bs);
            console.log(bs);
            console.log(bs);
            var rlen = 128*32 - bs.length;
            for (var i = 0; i<rlen; i++){
                bs.push(32);
            }
            this.t = [...Array(32)].map(e => Array(128));
            for (var lineid=0; lineid<32; lineid++){
                for (var cellid=0; cellid<128; cellid++){
                    this.t[lineid][cellid] = String.fromCharCode(bs[lineid*128+cellid]);
                }
            }
        }
    render() {
        this.initsys();
        this.assdemo(this.sadsu.tape);
        this.tdiu.refresh();
        var content = this.tdiu.content;
        return (
            <div className="App">
            YIWI
            <SimpleDisplay content={content}/>
            <SimpleDisplay className="prgmon" content={this.t}/>
            prg
            </div>
        );
  }
}

export default App;
