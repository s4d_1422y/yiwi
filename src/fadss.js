import Device from './Device';

class FADSS extends Device {
    constructor(id, size){
        super(id);
        this.values = [
            0, 0, 0, 0, // @10000 - @10003
            0,          // @10004
            0,          // @10005
            0,          // @10006
            0, 0,       // @10007, @10008
            0, 0        // @10009, @1000a: instruction pointer and curreent memory bank
        ]
    }
    processCommand(){
        switch (this.internalBuffer.pop()) {
            case 0x01: //peek, param: addr, id
                //setInterval(function () {
                    this.internalBuffer.push(this.values[this.internalBuffer.pop()-0x10000]);
                    this.preformTransaction(this.internalBuffer.pop());
            //    },10);
                break;
            case 0x02: //poke, param: addr. val,
            //    setInterval(function () {
                    this.values[this.internalBuffer.pop()-0x10000] = this.internalBuffer.pop();
                    console.log(this.values);
            //    },10);
                break;
            default:
                alert("ERR!: unsupported operation on device " + this.id + ". Dump: " + this.internalBuffer);
        }
    }
}
export default FADSS;
