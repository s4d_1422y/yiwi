import Deevice from './Device';

class IPU extends Device{
    consetuctor(id) {
        super(id);
    }
    /*16
    IPU connection protocol
        0: FADSS
        1: TDIU
        2: SADSU
        3: debug\test device
    */
    fetch(){
        this.internalBuffer.push(1);
        this.internalBuffer.push(0x10009); // Instruction pointer
        this.preformTransaction(0);
        this.connectionPool[0].acceptTransaction(this.id);
        this.preformTransaction(0);
        this.connectionPool[0].acceptTransaction(this.id);
        this.connectionPool[0].processCommand();
        this.acceptTransaction(0);
        var currentPtr = this.internalBuffer.pop();
        this.internalBuffer.push(1);
        this.internalBuffer.push(0x1000a); // bank
        this.preformTransaction(0);
        this.connectionPool[0].acceptTransaction(this.id);
        this.preformTransaction(0);
        this.connectionPool[0].acceptTransaction(this.id);
        this.connectionPool[0].processCommand();
        this.acceptTransaction(0);
        var currentBank = this.internalBuffer.pop();
        this.internalBuffer.push(1);
        this.internalBuffer.push(currentBank);
        this.preformTransaction(currentPtr);
        this.connectionPool[0].acceptTransaction(this.id);
        this.preformTransaction(0);
        this.connectionPool[0].acceptTransaction(this.id);
        this.connectionPool[0].processCommand();
        this.acceptTransaction(0);
        var currentByte = this.internalBuffer.pop();
    }
}
