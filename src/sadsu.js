import Device from './Device';

class SADSU extends Device {
    constructor(id, size){
        super(id);
        this.tapeLength = size;
        this.tape = Array(this.tapeLength).fill(32);
        this.currentAddressInBank = 0x0; // occupies h10
        this.currentBank = 0x0 // occupies h20
    }
    processCommand(){
        switch (this.internalBuffer.pop()) {
            case 0x01: //rewind to bank, addr, param: requested bank, requested address
            //    setTimeout(function () {
                    this.currentBank = this.internalBuffer.pop();
                    this.currentAddressInBank = this.internalBuffer.pop();
            //   },50);
                this.reset();
                break;
            case 0x02: //rewind to addr, current bank, param: requested address
            //    setTimeout(function () {
                    this.currentAddressInBank = this.internalBuffer.pop();
            //    },50);
                this.reset();
                break;
            case 0x03: //peek, param: id
                //setTimeout(function () {
                    this.internalBuffer.push(this.tape[this.currentBank*0xff+this.currentAddressInBank]);
                    this.preformTransaction(this.internalBuffer.pop());
            //    },50);
                this.reset();
                break;
            case 0x04: //poke, param: val
                //setTimeout(function () {
                    this.tape[this.currentBank*0xff+this.currentAddressInBank] = this.internalBuffer.pop();
                //},50);
                this.reset();
                break;
            default:
                alert("ERR!: unsupported operation on device " + this.id + ". Dump: " + this.internalBuffer);
        }
    }
}

export default SADSU;
