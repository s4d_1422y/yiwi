//import Device from './Device';

Object.defineProperty(Array.prototype, 'chunksOf', {
    value: function(chunkSize) {
        var R = [];
        for (var i=0; i<this.length; i+=chunkSize)
            R.push(this.slice(i,i+chunkSize));
        return R;
    }
});

class Ass {
    constructor(){
        this.arity2 = ['add', 'sub', 'idv', 'mul', 'and', 'bto', 'inv', 'xor', 'lsf', 'rsf', 'mov', 'cmp'];
        this.arity1 = ['sbr', 'jeq', 'jsr', 'jne', 'inc', 'dec', 'jgt', 'jls', 'smb', 'psh'];
        this.arity0 = ['hlt', 'nmb', 'pmb', 'pop'];
    }
    genOpCodes(){
        var opcodes = [];
        for (var i = 1; i < this.arity2.length; i++) {
            opcodes.push({'mnemonic' : this.arity2[i], 'opcode':
                            { 'numnum'   : i,
                              'numadr'   : ((i & 0b00111111)+0b10000000),
                              'adrnum'   : ((i & 0b00111111)+0b01000000),
                              'adradr'   : ((i & 0b00111111)+0b11000000)}}); //all like that
        }
        for (i = 0; i < this.arity1.length; i++) {
            opcodes.push({'mnemonic' : this.arity1[i], 'opcode':
                            { 'numnum'   : i+this.arity0.length,
                              'adradr'   : ((i & 0b00111111)+0b11000000)}});
        }
        for (i = 0; i < this.arity0.length; i++) {
            opcodes.push({'mnemonic' : this.arity0[i], 'opcode':
                            { 'numnum'   : i+this.arity2.length+this.arity1.length }});
        }
        return opcodes;
    }
    memify(sadsu, fadss, lines){
        //var conndev = new Device(16);
        //conndev.connect(sadsu);
        //sadsu.connect(conndev);
        //conndev.connect(fadss);
        //fadss.connect(conndev);
        return lines.map(function(line){ // from
            var raw = line.split(' ');
            return raw.map(function(elem) {
                var res = '';
                if (!elem.includes('^')) res+=elem;
                if (parseInt(elem.substring(1), 16) < 0x10000 && elem.includes('^')) {
                    res+=sadsu.tape[parseInt(elem.substring(1), 16)];
                }
                if (parseInt(elem.substring(1), 16) >= 0x10000 && elem.includes('^')){
                    res+=fadss.values[parseInt(elem.substring(1), 16) - 0x10000];
                }
                return res;
            });
        });
    } // to fixme
    toOpCodes(sourceStr){
        var lines = sourceStr.split('\n');
        var opcodes = this.genOpCodes();
        lines = lines.map(function(line) { var raw = line.split(' ');
                              var kind =    '';
                              if (raw.length === 3){
                                if (raw[1][0] === '@') {
                                    kind+='adr';
                                    raw[1] = raw[1].substring(1);
                                }
                                if (raw[1][0] === '^') {
                                    kind+='adr';

                                }
                                if (raw[1][0] === 'h') {
                                    raw[1] = parseInt(raw[1].substring(1), 16);
                                    kind+='num';
                                }
                                if (raw[1][0] === 'd') {
                                    raw[1] = raw[1].substring(1);
                                    kind+='num';
                                }
                                if (raw[1][0] === 'b') {
                                    raw[1] = parseInt(raw[1].substring(1), 2);
                                    kind+='num';
                                }
                                if (raw[1][0] === 'o') {
                                    raw[1] = parseInt(raw[1].substring(1), 8);
                                    kind+='num';
                                }
                                if (raw[2][0] === '@') {
                                    kind+='adr';
                                    raw[2] = raw[2].substring(1);
                                }
                                if (raw[2][0] === '^') {
                                    kind+='adr';
                                }
                                if (raw[2][0] === 'h') {
                                    kind+='num';
                                    raw[2] = parseInt(raw[2].substring(1), 16);
                                }
                                if (raw[2][0] === 'd') {
                                    kind+='num';
                                    raw[2] = raw[2].substring(1);
                                }
                                if (raw[2][0] === 'b') {
                                    kind+='num';
                                    raw[2] = parseInt(raw[2], 2);
                                }
                                if (raw[2][0] === 'o') {
                                    kind+='num';
                                    raw[2] = parseInt(raw[2], 8);
                                }
                            } else if (raw.length === 2) {
                                if (raw[1][0] === '@') {
                                    raw[1] = raw[1].substring(1);
                                    kind+='adradr';
                                }
                                if (raw[1][0] === '^') {
                                    kind+='adradr';
                                }
                                if (raw[1][0] === 'h') {
                                    raw[1] = parseInt(raw[1].substring(1), 16);
                                    kind+='numnum';
                                }
                                if (raw[1][0] === 'd') {
                                    raw[1] = raw[1].substring(1);
                                    kind+='numnum';
                                }
                                if (raw[1][0] === 'b') {
                                    raw[1] = parseInt(raw[1].substring(1), 2);
                                    kind+='numnum';
                                }
                                if (raw[1][0] === '0') {
                                    raw[1] = parseInt(raw[1].substring(1), 8);
                                    kind+='numnum';
                                }
                            } else if (raw.length === 1) kind = 'numnum';
                            var code = opcodes.find(function(instr){return (instr.mnemonic === raw[0])}).opcode[kind];
                            raw[0] = code;
                            return raw.join(' ');
                        });
        return lines;
    }
    bytify(bs) {
        var r = [];
        bs.map(elem => {
            if(elem <= 0xff){
                r.push(elem);
            } else {
                elem.split('').reverse().chunksOf(2).map(x => x.join('')).map(x => r.push(x));//.map((elem) => elem.map((subelem) => r.push(subelem)));
            }
        });
        console.log(r);
        return r;
    }
}

export default Ass;
