import Device from './Device';
class TDIU extends Device {
    constructor(id, videomem){
        super(id);
        this.videoMemory = videomem;
        this.content = [...Array(32)].map(e => Array(128));
    }
    refresh() {
        for (var lineid=0; lineid<32; lineid++){
            for (var cellid=0; cellid<128; cellid++){
                this.content[lineid][cellid] = String.fromCharCode(this.videoMemory[lineid*128+cellid]);
            }
        }
    }
}
export default TDIU;
